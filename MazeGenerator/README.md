# Maze Generator

An algorithm that allows you to generate 3D mazes in Unity3D.


## Getting Started

The class contained in the repositories allow to generate mazes and provide all a series of options to determine the structural characteristics.

### Prerequisites

The algorithm is written in C# in particular the standard version of .NET 2.0 has been used.

Unity 2019.3.9.f1 has been used for development but the code *should* work on all versions of Unity that support this version of .NET.

### Installing

To use the algorthm, follow the steps below:

* Create a new project in Unity3D or use an existing project.
* Copy the files "MazeGenerator.cs" in the project assets folder.
* Add the "MazeGenerator" component to any object in your scene.
* Enter all the parameters of the maze.
* Start the simulation or use the button "Generate Maze" in the inspector at runtime to generate a new maze in the scene.


## Usage

Once the component has been added to any object, the values must be entered in the inspector to generate the desired maze.

We will first have to enter the general values of our maze as:

* Posion of the maze
* Rotation of the maze
* Scaling of the maze

**Attention:** Keep in mind when entering data that the maze pivot is located in the **bottom left corner** of the generated structure.

Then the size of the maze must be entered.

Afterwards, through a particular check, we could decide whether to generate random mazes or whether to use a seed if the tick was not present.

It will also be possible to select the materials to be used for the maze floor or for the walls and the height of the walls.

Finally the last check box will be used to allow the user to choose whether to delete the mazes present in the scene when clicking on the "Generate Maze" button or simply to recreate a new one without touching the previously created mazes.
**Attention: To delete the mazes present in the scene, the algorithm assigns to each maze created the "Finish" tag already present in Unity to make the software work stand alone.
If you check the checkbox keep in mind that the algorithm could also delete other objects in the scene with the same "Finish" tag.**

![Final Result](./Images/Maze-Final.png "Final Result") 

The photo above shows a maze generated with this algorithm.

## Built With

* [Unity](https://docs.unity3d.com/Manual/index.html) - The game engine.
* [C#](https://docs.microsoft.com/it-it/dotnet/csharp/) - The language used.


## Versioning

I use [BitBucket](https://bitbucket.org/) for versioning. 



## Authors

* **Marco Picariello** 