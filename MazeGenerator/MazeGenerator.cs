﻿/*
 Author: Marco Picariello
 Language: C#
 Description: The code gives an algorithm for the generation of a 3D maze in Unity.
    The labyrinth is first generated on a numerical matrix for this efficiency and then visually shown.
*/

using UnityEngine;
using UnityEditor;

public class MazeGenerator : MonoBehaviour{

    private GameObject maze;

    //These variables are used for generate randomic or procedural maze.
    [Header("Button Settings")]
    [Tooltip("Doesn't do anything. Just comments shown in inspector")]
    public bool randomic;
    public int seedRandom;
    //These three variables are used to manage the position, rotation and scale of the maze.
    public Vector3 mazePosition = new Vector3(0,0,0);
    public Vector3 mazeRotation = new Vector3(0, 0, 0);
    public Vector3 mazeScale = new Vector3(1, 1, 1);
    
    public int mazeWidth;
    public int mazeHeight;

    public Material floorMaterial;
    public Material wallMaterial;

    public int wallHeight = 4;

    //This variable is used to allow the user to choose whether to destroy existing
    //mazes already when clicking on the "Generate Maze" button.
    public bool destroyMaze;

    //This matrix is used for generate the maze structure.
    private int[,] numericMaze;
   
    private GameObject floor;
    private GameObject wall;

    //These variables are used to generate the numeric maze.
    int horVer = 0;
    bool last = false;
    int countCut = 0;
    
    //These variables are used to set the position of the single blocks of the maze.
    private int xPos = 0;
    private int yPos = 0;

    //this variable is used to report any errors in the input parameters and prevent the generation of the maze.
    [HideInInspector]
    public bool canGenerate = false;

    private void Start() {
        GenerateMaze();
    }

    
    
    //This methos called at the Start and in the button "Generate Maze" of the inspector call in sequence all the methods useful for maze generation.
    public void GenerateMaze() {
        if (!canGenerate) {
            Debug.LogError("There are errors in the input data.");
            return;
        }
        if (!randomic) {
            Random.InitState(seedRandom);
        }
        if (destroyMaze) {
            GameObject[] mazes = GameObject.FindGameObjectsWithTag("Finish");
            foreach(GameObject instaMaze in mazes) {
                DestroyImmediate(instaMaze);
            }
        }

        numericMaze = new int[mazeWidth, mazeHeight];

        countCut = (int)Random.Range(0, 1001) % 2;

        GeneratePrefabs();
        GenerateNumericMaze(0, 0, mazeWidth - 1, mazeHeight - 1);
        GenerateRealMaze();
        GenerateBorders();
        MazeTransform();
        DestroyImmediate(wall);
        DestroyImmediate(floor);
    }

    //Generate the GameObjects for the construction of the maze: wall and floor.
    private void GeneratePrefabs() {
        GameObject wallProv;
        GameObject floorProv;

        floor = GameObject.CreatePrimitive(PrimitiveType.Plane);
        floor.GetComponent<Renderer>().material = floorMaterial;
        floor.transform.rotation = Quaternion.Euler(new Vector3(0, 0, 0));

        floorProv = GameObject.CreatePrimitive(PrimitiveType.Plane);
        floorProv.GetComponent<Renderer>().material = floorMaterial;
        wallProv = GameObject.CreatePrimitive(PrimitiveType.Cube);
        wallProv.transform.localScale = new Vector3(10, wallHeight, 1);
        wallProv.GetComponent<Renderer>().material = wallMaterial;
        wallProv.transform.position = new Vector3(0, wallHeight / 2, -5);
        wall = floorProv;
        wallProv.transform.parent = wall.transform;
        wall.transform.rotation = Quaternion.Euler(new Vector3(0, 0, 0));
    }

    /*Generate the maze structure using an array of numbers to increase efficiency.
    The algorithm decides whether to apply a horizontal or vertical subdivision and to 
    change the matrix of integers by inserting 1 or 2 where the horizontal and verti- cal walls are present.*/
    private void GenerateNumericMaze(int startx, int starty, int sizex, int sizey) {
        int xSub=0;
        int ySub=0;
        int doorCell =0;
        horVer = -1;

        if (startx == sizex && starty == sizey) {
            return;
        } else {  
            if (startx == sizex) {
               if (startx == 0) {
                    return;
                } else {
                    horVer = 0;
                    last = true;
                }              
            }
            if (starty == sizey) {
                if (starty == 0) {
                    return;
                } else {
                    horVer = 1;
                    last = true;
                }            
            }
        }
        if (horVer == -1) {
            if (countCut == 0) {
                horVer = 1;
                countCut = 1;
            } else {
                horVer = 0;
                countCut = 0;
            }
        }
        if (horVer == 0) {
            if (last) {
                xSub = Random.Range(startx, sizex + 1);
            } else {
                xSub = Random.Range(startx + 1, sizex + 1);
            }          
            for (int i = starty; i < sizey+1; i++) {
                numericMaze[xSub, i] = 1;
            }          
            doorCell = Random.Range(starty, sizey+1);
            numericMaze[xSub, doorCell] = 0;
            if (startx <= xSub-1 && starty <= sizey) {
                GenerateNumericMaze(startx, starty, xSub-1, sizey);
            }
            if (xSub+1 <= sizex && starty <= sizey) {
                GenerateNumericMaze(xSub+1, starty, sizex, sizey);
            }     
        } else {
            if (last) {
                ySub = Random.Range(starty, sizey + 1);
            } else {
                ySub = Random.Range(starty+1, sizey+1);
            }        
            for (int i = startx; i < sizex+1; i++) {
                numericMaze[i, ySub] = 2;
            }
            doorCell = Random.Range(startx, sizex+1);
            numericMaze[doorCell, ySub] = 0;
            if (startx <= sizex && starty <= ySub-1) {
                GenerateNumericMaze(startx, starty, sizex, ySub-1);
            }
            if (startx <= sizex && ySub + 1 <= sizey) {
                GenerateNumericMaze(startx, ySub + 1, sizex, sizey);
            }    
        }

    }

    //This method converts the array of integers into the actual labyrinth by assigning the related GameObjects.
    private void GenerateRealMaze() {
        maze = new GameObject("Maze");
        maze.tag = "Finish";
        GameObject prov;
        xPos = 0;
        yPos = 0;
        for (int i = 0; i < mazeWidth; i++) {
            for (int j = 0; j < mazeHeight; j++) {
                if (numericMaze[i, j] == 0) {
                    prov = Instantiate(floor, new Vector3(xPos, 0, yPos), Quaternion.identity);
                    xPos += 10;
                    prov.transform.parent = maze.transform;
                } else if (numericMaze[i, j] == 1) {
                    prov = Instantiate(wall, new Vector3(xPos, 0, yPos), Quaternion.Euler(new Vector3(0, 0, 0)));
                    xPos += 10;
                    prov.transform.parent = maze.transform;
                } else if (numericMaze[i, j] == 2) {
                    prov = Instantiate(wall, new Vector3(xPos, 0, yPos), Quaternion.Euler(new Vector3(0, 90, 0)));
                    xPos += 10;
                    prov.transform.parent = maze.transform;
                }
            }
            xPos = 0;
            yPos += 10;
        }
    }

    //This method generates the walls at the edge of the maze.
    private void GenerateBorders() {
        GameObject wall;
        
        //I create the left wall.
        wall = GameObject.CreatePrimitive(PrimitiveType.Cube);
        wall.transform.localScale = new Vector3(mazeWidth * 10, wallHeight, 1);
        wall.transform.rotation = Quaternion.Euler(new Vector3(0, 90, 0));
        wall.GetComponent<Renderer>().material = wallMaterial;
        wall.transform.position = new Vector3(-5, wallHeight/2, ((mazeWidth * 10) / 2) - 5);
        wall.transform.parent = maze.transform;
        
        //I create the right wall.
        wall = GameObject.CreatePrimitive(PrimitiveType.Cube);
        wall.transform.localScale = new Vector3(mazeWidth * 10, wallHeight, 1);
        wall.GetComponent<Renderer>().material = wallMaterial;
        wall.transform.rotation = Quaternion.Euler(new Vector3(0, 90, 0));
        wall.transform.position = new Vector3((mazeHeight * 10) - 5, wallHeight/2, ((mazeWidth* 10) / 2) - 5);
        wall.transform.parent = maze.transform;
        
        //I create the lower wall.
        wall = GameObject.CreatePrimitive(PrimitiveType.Cube);
        wall.transform.localScale = new Vector3(mazeHeight * 10, wallHeight, 1);
        wall.GetComponent<Renderer>().material = wallMaterial;
        wall.transform.position = new Vector3(((mazeHeight * 10) / 2) - 5, wallHeight/2, -5);
        wall.transform.parent = maze.transform;
        
        //I create the upper wall.
        wall = GameObject.CreatePrimitive(PrimitiveType.Cube);
        wall.transform.localScale = new Vector3(mazeHeight * 10, wallHeight, 1);
        wall.GetComponent<Renderer>().material = wallMaterial;
        wall.transform.position = new Vector3(((mazeHeight * 10) / 2) - 5, wallHeight/2, ((mazeWidth * 10)) - 5);
        wall.transform.parent = maze.transform;
    }

    //Apply transformations to the whole labyrinth.
    private void MazeTransform() {
        maze.transform.position = mazePosition;
        maze.transform.rotation = Quaternion.Euler(mazeRotation);
        maze.transform.localScale = mazeScale;
    }
}


//This class implement a custom inspector for a more intuitive interaction with the generator.
[CustomEditor(typeof(MazeGenerator))]
public class MyScriptEditorMaze : Editor {
    MazeGenerator myScript;

    void OnEnable() {
        myScript = (MazeGenerator)target;
    }

    public override void OnInspectorGUI() {
        myScript.mazePosition = EditorGUILayout.Vector3Field("Maze Position", myScript.mazePosition);
        myScript.mazeRotation = EditorGUILayout.Vector3Field("Maze Rotation", myScript.mazeRotation);
        myScript.mazeScale = EditorGUILayout.Vector3Field("Maze Scale", myScript.mazeScale);
        EditorGUILayout.Space();

        myScript.mazeHeight = EditorGUILayout.IntField("Maze Height", myScript.mazeHeight);
        myScript.mazeWidth = EditorGUILayout.IntField("Maze Width", myScript.mazeWidth);
        EditorGUILayout.Space();

        myScript.randomic = GUILayout.Toggle(myScript.randomic, "Randomic Generation");
        if (!myScript.randomic) {
            myScript.seedRandom = EditorGUILayout.IntField("Random Seed", myScript.seedRandom);
        }
        EditorGUILayout.Space();


        myScript.floorMaterial = (Material)EditorGUILayout.ObjectField("Floor Material", myScript.floorMaterial, typeof(Material), false);
        myScript.wallMaterial = (Material)EditorGUILayout.ObjectField("Wall Material", myScript.wallMaterial, typeof(Material), false);
        EditorGUILayout.Space();

        myScript.wallHeight = EditorGUILayout.IntField("Walll Height", myScript.wallHeight);
        EditorGUILayout.Space();

        myScript.destroyMaze = GUILayout.Toggle(myScript.destroyMaze, "Destroy Existing Mazes");
        EditorGUILayout.Space();

        if (myScript.mazeWidth <= 0 || myScript.mazeHeight <= 0) {
            EditorGUILayout.HelpBox("Error: Wrong value for maze size.", MessageType.Error);
            myScript.canGenerate = false;
        }else if (myScript.wallHeight <= 0) {
            EditorGUILayout.HelpBox("Error: Wrong value for wall size.", MessageType.Error);
            myScript.canGenerate = false;
        } else if (myScript.wallMaterial == null || myScript.floorMaterial == null) {
            EditorGUILayout.HelpBox("Error: Unspecified floor and/or wall materials.", MessageType.Error);
            myScript.canGenerate = false;
        } else if (myScript.mazeHeight*myScript.mazeWidth >= 100000) {
            EditorGUILayout.HelpBox("Warning: Generate too large mazes may impact performance.", MessageType.Warning);
            myScript.canGenerate = true;
        } else {
            myScript.canGenerate = true;
        }

        if (GUILayout.Button("Generate Maze")) {
            myScript.GenerateMaze();
        }

    }
}



